# CosmiX

Cosmic ray model for astronomical imaging detectors

## Install

### From Pip

```
pip install cosmix
```

### From Conda

```
conda install dask
```

### Install from Source

```
git clone
cd cosmix
pip install -e .
```
